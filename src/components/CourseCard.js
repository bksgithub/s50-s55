import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import PropTypes from "prop-types"
import {useState} from "react"
import {useEffect} from "react"
import {Link} from "react-router-dom"

function CourseCard({course}) {
  //destructuring the props
  const {name, description, price, _id} = course

  //using the state
  //initialize a count state with a value of zero (0)
  const [count, setCount] = useState(0)

  const [slots, setSlot] = useState(15)

  const [isOpen, setIsOpen] = useState(true)

  function enroll() {
    // setCount(count + 1)
  // setCount(count => Math.max(count + 1, 0));


  // if (count > 15) {
  //   return
  // }

  //   setSlot(slots => Math.max(slots - 1, 0));

  //   if (slots === 0) {
  //     alert("No more slots for this course.")
  //   }

  //   if (slots > 0) {
  //   setCount(count + 1)
  //   setSlot(slots - 1)
  // } else {
  //   alert("No more slots for this course.")
  // }
  }

//effects is react is just like side effects/effects in real life where everytime something happens within the component, a function or condition runs
//You may also listen or watch a specific state of changes instead of watching /listening to the whole component
useEffect(() => {
  if (slots === 0) {
    setIsOpen(false)
  }
}, [slots])


  return (
    <Card style={{ width: "100%"}}>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle className="">Description:</Card.Subtitle>
        <Card.Text>
          {description}
        </Card.Text>
        <Card.Subtitle className="">Price:</Card.Subtitle>
        <Card.Text>
          PHP {price}
        </Card.Text>
        <Card.Text>
          Enrollees: {count}
        </Card.Text>
        <Card.Text>
          Slots: {slots}
        </Card.Text>
        <Card.Text>
          Is Open: {isOpen ? "Yes" : "No"}
        </Card.Text>
        {/*<Button variant="primary" onClick = {enroll}>Enroll</Button>*/}
        <Link className = "btn btn-primary" to = {`/courses/${_id}`}>Details</Link>
      </Card.Body>
    </Card>
  );
}

export default CourseCard;

// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them
CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}