import './App.css';
import AppNavbar from "./components/AppNavbar"
// import Banner from "./components/Banner"
// import Highlights from "./components/Highlights"
import {Container} from "react-bootstrap"
import Home from "./pages/Home"
import Courses from "./pages/Courses"
import Register from "./pages/Register"
import Login from "./pages/Login"
import {BrowserRouter as Router, Route, Routes} from "react-router-dom"
import Logout from "./pages/Logout"
import ErrorPage from "./pages/ErrorPage"
import {useState} from "react"
import {UserProvider} from "./UserContext"
import CourseView from "./components/CourseView"


function App() {

const [user, setUser] = useState({
  id: null,
  isAdmin: null
})

const unsetUser = () => {
  localStorage.clear()
}

  return (
    <>
  {/*provides the user context throughout any component inside of it*/}
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
      <AppNavbar/>
    <Container>
      {/*<Home/>
      <CourseCard/>*/}
      {/*<Register/>*/}
      {/*<Login/>*/}
      <Routes>
        <Route path = "/" element = {<Home/>}/>
        <Route path = "/courses" element = {<Courses/>}/>
        <Route path = "/login" element = {<Login/>}/>
        <Route path = "/register" element = {<Register/>}/>
        <Route path = "/logout" element = {<Logout/>}/>
        <Route path="*" element={<ErrorPage/>} />
        <Route path = "/courses/:courseId" element = {<CourseView/>}/>
      </Routes>
    </Container>
    </Router>
    </UserProvider>
    {/*initializes that dynamic routing will be involved.*/}
    </>
  )
}

export default App;
