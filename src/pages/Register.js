import {Form, Button} from "react-bootstrap"
import {useState, useEffect, useContext} from "react"
import UserContext from "../UserContext"
import {Navigate, useNavigate} from "react-router-dom"
import Swal from "sweetalert2"

export default function Register() {

	const {user, setUser} = useContext(UserContext)
	const [email, setEmail] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState("")
	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [mobileNumber, setMobileNumber] = useState("")
	//for determining if button is disabled or not
	const[isActive, setIsActive] = useState(false)
	const navigate = useNavigate()

function registerUser(event) {
	event.preventDefault()

	fetch(`${process.env.REACT_APP_API_URL}/users/check-email`, {
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			email: email
		})
	}).then(response => response.json()).then(result => {
		if (result === true) {
			Swal.fire({
				title: "Oops!",
				icon: "error",
				text: "Email is already in use."
			})
		} else {
			fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					mobileNo: mobileNumber,
					password: password1
				})
			}).then(response => response.json()).then(result => {
				if (result !== false) {

					setEmail("")
					setPassword1("")
					setPassword2("")
					setFirstName("")
					setLastName("")
					setMobileNumber("")

					Swal.fire({
				title: "Success!",
				icon: "success",
				text: "Register successful."
			})
					navigate("/login")
				} else {
					Swal.fire({
				title: "Sorry",
				icon: "error",
				text: "Something's not right."
			})
				} 
			})
		}
	})

	//clears out the input fields after form submission
	setEmail("")
	setPassword1("")
	setPassword2("")
	setFirstName("")
	setLastName("")
	setMobileNumber("")

}

	useEffect(() => {
		if ((mobileNumber.length === 11 && lastName !== "" && firstName !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
			//enables the submit button if the form data has been verified
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password1, password2, firstName, lastName, mobileNumber])

	return (

		(user.id !== null) ? <Navigate to = "/courses"></Navigate> : <Form onSubmit = {event => registerUser(event)}>

		<Form.Group controlId="firstName">
		   <Form.Label>First Name</Form.Label>
		   <Form.Control 
		    type="text" 
		    placeholder="Enter First Name"
		    value = {firstName}
		    onChange = {event => setFirstName(event.target.value)} 
		    required
		   />
		</Form.Group>
		<Form.Group controlId="lastName">
		   <Form.Label>Last Name</Form.Label>
		   <Form.Control 
		    type="text" 
		    placeholder="Enter Last Name"
		    value = {lastName}
		    onChange = {event => setLastName(event.target.value)} 
		    required
		   />
		</Form.Group>
		<Form.Group controlId="userEmail">
		   <Form.Label>Email address</Form.Label>
		   <Form.Control 
		    type="email" 
		    placeholder="Enter email"
		    value = {email}
		    onChange = {event => setEmail(event.target.value)} 
		    required
		   />
		   <Form.Text className="text-muted">
		       We'll never share your email with anyone else.
		   </Form.Text>
		</Form.Group>
		<Form.Group controlId="password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
			    type="password" 
			    placeholder="Password"
			    value = {password1}
		    onChange = {event => setPassword1(event.target.value)}
			    required
		    />
		</Form.Group>
		<Form.Group controlId="password2">
		   <Form.Label>Verify Password</Form.Label>
		   <Form.Control 
		    type="password" 
		    placeholder="Verify Password" 
		    value = {password2}
		    onChange = {event => setPassword2(event.target.value)}
		    required
		   />
		</Form.Group>
		<Form.Group controlId="mobileNumber">
		   <Form.Label>Mobile Number</Form.Label>
		   <Form.Control 
		    type="text" 
		    placeholder="Enter Mobile number"
		    value = {mobileNumber}
		    onChange = {event => setMobileNumber(event.target.value)} 
		    required
		   />
		</Form.Group>

		{
			isActive ? <Button variant="primary" type="submit" id="submitBtn">
		            	Submit
		</Button> : <Button variant="primary" type="submit" id="submitBtn" disabled>
		            	Submit
		</Button>
		}
	</Form>
	)
}