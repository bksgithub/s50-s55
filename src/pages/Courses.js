import CourseCard from "../components/CourseCard"
// import courses_data from "../data/courses"
import {useEffect, useState} from "react"
import Loading from "../components/Loading"

export default function Courses() {

const [courses, setCourses] = useState([])
const [isLoading, setIsLoading] = useState(false)

	useEffect(() => {
		//sets the loading state to true
		setIsLoading(true)

		fetch(`${process.env.REACT_APP_API_URL}/courses/`).then(response => response.json()).then(result => {
			setCourses(
				result.map((course) => {
					return (
						<CourseCard key = {course._id} course = {course}/>
					)
				})
			) 
			//sets loading state to false
			setIsLoading(false)
		})
	}, [])

	return (
		
			(isLoading) ? <Loading/> : 
			<>
			{courses} 
			</>
	)
}