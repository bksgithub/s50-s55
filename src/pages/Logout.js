import {Navigate} from "react-router-dom"
import UserContext from "../UserContext"
import {useEffect, useContext} from "react"
// import {useNavigate} from "react-router-dom"

export default function Logout() {
	const {unsetUser, setUser} = useContext(UserContext)
	//using the context, clear the contents of the local storage
	unsetUser()

	//an effect which removes the user email from the global user state that comes from the context
	useEffect(() => {
		setUser({
			id: null
		})
	}, [])


	return (
		<Navigate to = "/login"/>
	)
}